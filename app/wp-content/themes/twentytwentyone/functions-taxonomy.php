<?php
// function register_taxonomy_wrapper( $taxonomy, $args = array() ) {
//   $text_domain = 'twentytwentyone';
//   // \wp-includes\taxonomy.php->get_taxonomy_labels():$nohier_vs_hier_defaults
//   $labels = array(
//     'name' => _x( $taxonomy, 'taxonomy general name', $text_domain ),
//   );

//   $default = array(
//     'post_type' => 'post',
//     'labels'    => $labels,
//   );
//   $args = array_merge( $default, $args );

//   register_taxonomy( $taxonomy, $args['post_type'], $args );
// }

// function like_taxonomy() {
//   $args = array();
//   register_taxonomy_wrapper( 'like', $args );
// }
// add_action( 'init', 'like_taxonomy' );


function override_put_wti_like_post( $content ) {
  return $content;
}

function target_main_category_query_with_conditional_tags( $query ) {
  remove_filter( 'the_content', 'PutWtiLikePost' );
  add_filter( 'the_content', 'override_put_wti_like_post' );
}
add_action( 'pre_get_posts', 'target_main_category_query_with_conditional_tags' );
