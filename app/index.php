<?php
/**
 * Front to the WordPress application. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *
 * @package WordPress
 */

/**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */
define( 'WP_USE_THEMES', true );

/** Loads the WordPress Environment and Template */
require __DIR__ . '/wp-blog-header.php';

$args = [
  'post_type'           => 'post',
  'post_status'         => 'publish',
  'orderby'             => 'post_date',
  'order'               => 'DESC',
  'posts_per_page'      => -1, // All.
  'ignore_sticky_posts' => 1, // Not moving the posts to begin of the set.
  'no_found_rows'       => 1, // No ->found_posts|max_num_pages.
  'fields'              => 'ids', // Optional, no cache.
  'tax_query'           => [
    'include_children' => 0, //
    'operator'         => 'EXISTS', // Get posts by taxonomy.
  ]
];

$args = [
  'taxonomy'   => 'category',
  'parent'     => 0, // Lv2 terms;
  'child_of'   => 0, // Lv2+ terms.
  'hide_empty' => false, // For rewrite only.
  'orderby'    => 'parent', // Parent first then child;
  'order'      => 'DESC', // Should have been ASC.
];
