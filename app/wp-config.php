<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

 // Added by WP Rocket

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'o||&62sS46m)Y6ZjUYxEQ drg{U8iclDQnO_U@7Me>)?T?>N{-xwr}hboCSZ)3t>' );
define( 'SECURE_AUTH_KEY',  'x!r3XKN/KR$?f5bCPNC,w3I3!/b<TwXnD%iz*)l/~3,r~Y~PVcoTH,^|Xy55jV>u' );
define( 'LOGGED_IN_KEY',    '~z!-$D@CQwMn|ls6+Tq(tCPh_b8HK1^)c!PEuTZ6Qm]`PTHK/$$@%}&.HAx8rfPj' );
define( 'NONCE_KEY',        'PnQi?6=9l-VyGMH:^[VR;w^z-Jl|k2mB.DW<KXeR%g]0B}D^oop,5~rEK3z{X7DC' );
define( 'AUTH_SALT',        'b=EAwDu$z6tOb9N ji+p+V#v$R|eO|9NPoHELawQQ*Ehf#}WN|%4?~WyXp2l5D~~' );
define( 'SECURE_AUTH_SALT', 'd?:b`zX)dhJgQ-I*`$T1JA@!]~aL>73a4pym/~wHvai.]OxHO`f!X?/z>73,|;Zd' );
define( 'LOGGED_IN_SALT',   'X kn(#)zj%V!fmpCSu}MR`cEL~H(4p+??V>h8(Ck!F]:2gO<CQ1gMns8.S(3QF{$' );
define( 'NONCE_SALT',       'pwC#BA3G#P/LlEqxWm{KqI9kweU))`{7VwqzX`zrD|#d]Lt.^mm41Q;%eZBypA]_' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */

require_once (__DIR__ . '/../vendor/autoload.php');
// define( 'WP_DEBUG_LOG', true );
// define( 'SCRIPT_DEBUG', true );

define( 'WP_POST_REVISIONS', false );
define( 'WP_DEFAULT_THEME', 'twentytwentyone' );

// define( 'WP_CONTENT_DIR', __DIR__ . '/app/content' );
// define( 'WP_CONTENT_URL', 'https://' . $_SERVER['HTTP_HOST'] . '/app/content' );

// define('WP_PLUGIN_DIR', 'new/path/to/plugins');
// define('WP_PLUGIN_URL', '/url/to/plugins');

// define( 'SHORTINIT', true ); // wp, wp_query, wp_the_query are null

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
